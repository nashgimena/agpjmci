var jmciRoute = angular.module('jmciRoute', ['ngRoute']);

jmciRoute.config(function ($routeProvider, $locationProvider){
	$routeProvider
		.when('/', {
			controller: 'MainController',
			templateUrl: 'pages/home.html'
		})
		.when('/about', {
			controller: 'AboutController',
			templateUrl: 'pages/about.html'
		})
		.when('/services', {
			controller: 'ServicesController',
			templateUrl: 'pages/services.html'
		})
		.when('/clients', {
			controller: 'ClientsController',
			templateUrl: 'pages/clients.html'
		})
		.when('/affiliates', {
			controller: 'AffiliatesController',
			templateUrl: 'pages/affiliates.html'
		})
		.when('/contact', {
			controller: 'ContactController',
			templateUrl: 'pages/contact.html'
		});
	$locationProvider.html5Mode(true);
});