var jmciApp = angular.module('jmciApp', ['ngRoute', 'jmciRoute', 'ngSanitize']);

jmciApp.controller('MainController', function ($scope){
		$scope.hoverAffliateClientIn = function() {
			this.showAffliatesClients = true;
		}

		$scope.hoverAffliateClientOut = function() {
			this.showAffliatesClients = false;
		}
});

jmciApp.controller('AboutController', function ($scope){
});

jmciApp.controller('AboutController', function ($scope){
});

jmciApp.controller('ServicesController', function ($scope){
	$scope.services = [
		{
			"company": "<b>SAMSUNG ELEC.-MECHA. PHIL., CORP.</b>",
			"address": "Brgy. Batino, Calamba Laguna",
			"products": [
				"4 Units <b>LG</b> Freight Elevator",
				"2 Units <b>HYUNDAI</b> Freight Elevator",
				"2 Units Dumbwaiter",
				"1 Unit Hydraulic Lifter"
			]
		},
		{
			"company": "<b>LUFTHANSA TECHNIK PHILS., INC.</b>",
			"address": "Villamor Airbase, Pasay City",
			"products": [
				"7 Units <b>HYUNDAI</b> Passenger/Freight Elevator",
				"1 Unit Dumbwaiter",
			]
		},
		{
			"company": "<b>AMANG RODRIGUEZ HOSPITAL</b>",
			"address": "Marikina City",
			"products": [
				"1 Unit <b>OTIS</b> Hospital Bed Elevator",
				"1 Unit <b>KOYO</b> Hospital Bed Elevator",
				"1 Unit <b>SIGMA</b> Hospital Bed Elevator",
				"1 Unit <b>NIPPON</b> Hospital Bed Elevator"
			]
		},
		{
			"company": "<b>PILIPINAS TELESERV</b>",
			"address": "Quiapo Manila",
			"products": [
				"1 Unit <b>FUJI</b> Passenger Elevator"
			]
		},
		{
			"company": "<b>HOTEL PARADIS</b>",
			"address": "Sta. Cruz Manila",
			"products": [
				"1 Unit <i>China Brand</i> Passenger Elevator"
			]
		},
		{
			"company": "<b>PHILIPPINE PORTS AUTHORITY</b>",
			"address": "Fu-Yong Mansion Condominium 913 Ongpin St., Binondo Manila",
			"products": [
				"1 Unit <b>KONE</b> Passenger Elevator",
				"2 Units <b>HITACHI</b> Passenger Elevator"
			]
		},
		{
			"company": "<b>QUICKSERVE (KFC)</b>",
			"address": "Quezon City",
			"products": [
				"10 Units Dumbwaiter",
			]
		},
		{
			"company": "<b>RUSTAN COFFEE CORP.</b>",
			"address": "Starbucks Robinsons Pioneer",
			"products": [
				"1 Unit Dumbwaiter",
			]
		},
		{
			"company": "<b>FIRST LANDLINK ASIA DEVELOPEMENT</b>",
			"address": "Libertad, Pasay City",
			"products": [
				"6 Units <b>HYUNDAI</b> Escalator",
				"1 Unit <b>HYUNDAI</b> Passenger Elevator"
			]
		},
		{
			"company": "<b>MATERRCO INC.</b>",
			"address": "Libertad, Pasay City",
			"products": [
				"4 Units <b>HYUNDAI</b> Escalator",
			]
		},
		{
			"company": "<b>PEARL PLAZA MALL</b>",
			"address": "Tambo, Paranaque City",
			"products": [
				"1 Unit <b>Mitsubishi</b> Scenic Elevator",
				"4 Units <b>Mitsubishi</b> Freight Elevator",
				"20 Units <b>Mitsubishi</b> Escalator"

			]
		},
		{
			"company": "<b>MASAGANA SUPER STORE</b>",
			"address": "Kalaw, Manila",
			"products": [
				"8 Units <b>Gold Star</b> Escalator",
			]
		},
		{
			"company": "<b>SHOPPER GOLD</b>",
			"address": "Avenida, Manila City",
			"products": [
				"4 Units <b>SCHINDLER</b> Escalator",
				"2 Units <b>SCHINDLER</b> Passenger Elevator"
			]
		},
		{
			"company": "<b>EMPIRE CENTER MALL</b>",
			"address": "Pasay City",
			"products": [
				"1 Unit <b>HYUNDAI</b> Passenger Elevator",
				"8 Units <b>HYUNDAI</b> Escalator"
			]
		},
		{
			"company": "<b>NATIONAL BOOKSTORE</b>",
			"address": "Quezon City",
			"products": [
				"4 Units <b>HYUNDAI</b> Freight Elevator",
				"6 Units <b>HYUNDAI</b> Escalator"
			]
		},
		{
			"company": "<b>ANGELES SAVER&rsquo;S MALL</b>",
			"address": "Angeles, Pampanga",
			"products": [
				"1 Unit <b>HYUNDAI</b> Passenger Elevator",
				"2 Units <b>HYUNDAI</b> Escalator"
			]
		},
		{
			"company": "<b>MEGA CENTER</b>",
			"address": "Cabanatuan City",
			"products": [
				"1 Unit <b>HYUNDAI</b> Freight Elevator",
				"2 Units <b>HYUNDAI</b> Passenger Elevator",
				"10 Units <b>HYUNDAI</b> Escalator"
			]
		},
		{
			"company": "<b>STAR MALL ANNEX</b>",
			"address": "Shaw Blvd. Mandaluyong City",
			"products": [
				"2 Units <b>HYUNDAI</b> Scenic Elevator",
				"8 Units <b>HYUNDAI</b> Escalator",
				"4 Units <b>LG</b> Escalator"
			]
		},
		{
			"company": "<b>VICTORY CENTRAL MALL</b>",
			"address": "Caloocan City",
			"products": [
				"2 Units <b>LG</b> Scenic Elevator",
			]
		},
		{
			"company": "<b>SHOPWISE</b>",
			"address": "Cubao, Quezon City",
			"products": [
				"2 Units <b>FUJI</b> Walkalator"
			]
		},
		{
			"company": "<b>SHOPWISE</b>",
			"address": "Commonwealth Quezon City",
			"products": [
				"4 Units <b>FUJI</b> Walkalator"
			]
		},
		{
			"company": "<b>SEA WINGS REALTY</b>",
			"address": "C.M. Recto Manila",
			"products": [
				"1 Unit <b>HYUNDAI</b> Passenger Elevator"
			]
		},
		{
			"company": "<b>RUSTANS SHANGRILA</b>",
			"address": "Mandaluyong City",
			"products": [
				"2 Units <b>SJEC</b> Walkalator"
			]
		},
		{
			"company": "<b>DIAMOND ARCADE MALL</b>",
			"address": "Cubao, Quezon City",
			"products": [
				"1 Unit <b>HYUNDAI</b> Passenger Elevator",
				"4 Units <b>HYUNDAI</b> Escalator"
			]
		}
	];
});

jmciApp.controller('ClientsController', function ($scope){
	$scope.clients = [
		{
			"company": "<b>LUFTHANSA TECHNIK PHIL. INC.</b>",
			"address": "Villamor Airbase, Pasay City",
			"products": [
				"1 Unit / 4 Stops <b>GREAT</b> Freight Elevator"
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>AMANG RODRIGUEZ HOSPITAL</b>",
			"address": "Marikina City",
			"products": [
				"1 Unit / 8 Stops <b>GREAT</b> Hospital Bed Elevator",
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>SOUTHERN LUZON STATE UNIVERSITY</b>",
			"address": "Brgy. Kulapi, Lucban Quezon",
			"products": [
				"1 Unit / 5 Stops <b>GREAT</b> Passenger Elevator",
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>AIDA GARCIA RESIDENCE</b>",
			"address": "San Juan City",
			"products": [
				"1 Unit / 4 Stops <b>GREAT</b> Homelift Elevator"
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>LA OBIEN PROPERTIES MANAGEMENT CORP.</b>",
			"address": "Lucena City",
			"products": [
				"1 Unit / 4 Stops <b>GREAT</b> Homelift Elevator"
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>CAMPO SAGRADO BLDG.</b>",
			"address": "Sta. Mesa Manila",
			"products": [
				"1 Unit / 5 Stops <b>GREAT</b> Passenger Elevator"
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>SINALOAN PUBLIC MARKET</b>",
			"address": "Sinaloan Laguna",
			"products": [
				"1 Unit / 3 Stops <b>GREAT</b> Scenic Passenger Elevator",
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>CHLEI CONST. & DEV&rsquo;T. INC.</b>",
			"address": "Brgy. Do&#241;a Jose Quezon City",
			"products": [
				"1 Unit / 5 Stops <b>GREAT</b> Passenger Elevator",
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>GLOBE ELECTRONIC BLDG.</b>",
			"address": "Quezon Blvd. Quiapo Manila",
			"products": [
				"1 Unit / 4 Stops <b>GREAT</b> Passenger Elevator"
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>NEWCORE INDUSTRIES INT&rsquo;L., INC</b>",
			"address": "1759 Nicanor Garcia St., Brgy. Poblacion Makati City",
			"products": [
				"1 Unit / 5 Stops <b>GREAT</b> Freight Elevator"
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>GP GENESIS DIALYSIS CLINIC DIAGNOSTIC CENTER</b>",
			"address": "East Ave. Quezon City",
			"products": [
				"1 Unit / 6 Stops <b>GREAT</b> Passenger Elevator",
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>1st DYNAMIC PERSONNEL RESOURCES INC.</b>",
			"address": "Do&#241;a Anita Bldg. E. Rodriguez Quezon City",
			"products": [
				"1 Unit / 5 Stops <b>GREAT</b> Passenger Elevator",
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>STAR CRUISES CENTER</b>",
			"address": "100 Andrews Ave., Newport Blvd. Pasay City",
			"products": [
				"6 Units / 9 Stops <b>GREAT</b> Passenger Elevator"
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>NAVOTAS GENERAL HOSPITAL</b>",
			"address": "Navotas City",
			"products": [
				"1 Unit / 3 Stops <b>GREAT</b> Hospital Bed Elevator"
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>NORTHFIELD LABORATORIES INC.</b>",
			"address": "FBIC, Malolos Bulacan",
			"products": [
				"1 Unit / 3 Stops <b>GREAT</b> Freight Elevator",
				"1 Unit Freight Elevator Change Control"
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>LYOD LABORATORIES INC.</b>",
			"address": "FBIC, Malolos Bulacan",
			"products": [
				"1 Unit / 2 Stops <b>GREAT</b> Freight Elevator"
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>STO. CRISTO PARISH MULTI-PURPOSE COOP.</b>",
			"address": "Marulas, Valenzuela City",
			"products": [
				"1 Unit / 4 Stops <b>GREAT</b> Scenic Passenger Elevator"
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>PHILIPPINE RED CROSS</b>",
			"address": "EDSA cor. Boni Mandaluyong",
			"products": [
				"1 Unit / 3 Stops <b>AGP</b> Dumbwaiter",
				"1 Unit / 2 Stops <b>AGP</b> Wheelchair Lift",
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>GSIS</b>",
			"address": "CCP Complex Roxas Blvd. Pasay City",
			"products": [
				"1 Unit / 7 Stops <b>KOYO</b> Passenger Elevator",
				"2 Units <b>KOYO</b> Escalator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>PACIFIC PAINT BOYSEN PHILIPPINES</b>",
			"address": "Del Monte Q.C. Branch",
			"products": [
				"1 Unit / 4 Stops <b>GREAT</b> Freight Elevator"
			],
			"services": "Supply / Installation / Maintenance"
		},
		{
			"company": "<b>MAKATI MEDICAL CENTER</b>",
			"address": "Makati City",
			"products": [
				"1 Unit /11 Stops <b>GREAT</b> Freight / Passenger Elevator"
			],
			"services": "Modernization / Installation / Maintenance"
		},
		{
			"company": "<b>NUCITI CENTRAL PROPERTIES & DEV&rsquo;T CORP.</b>",
			"address": "Tanauan, Batangas City",
			"products": [
				"4 Units / 5 Stops <b>AGP</b> Freight Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>TJ MARC SALES CORP. / CITIMART CAEDO</b>",
			"address": "Caedo, Pallocan St., Batangas City",
			"products": [
				"1 Unit / 2 Stops <b>AGP</b> Freight Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>OCM STEEL CORPORATION</b>",
			"address": "Marlboro cor. Winston St. Fairview Q.C.",
			"products": [
				"1 Unit / 4 stops <b>GREAT</b> Scenic Passenger Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>DON CORNELIO AWA BLDG.</b>",
			"address": "Tayuman St., Tondo Manila",
			"products": [
				"1 Unit / 6 stops <b>GREAT</b> Passenger Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>NEW VIC CONSTRUCTION CORP.</b>",
			"address": "Congressional Ave. Quezon City",
			"products": [
				"1 Unit / 5 stops <b>GREAT</b> Passenger Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>V. LUNA GENERAL HOSPITAL</b>",
			"address": "V. Luna Ave., Quezon City",
			"products": [
				"5 Units / 7 stops <b> KOYO</b> Hospital Bed Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>NEW CAPITOL TIRE TRADING CORP.</b>",
			"address": "East Drive, Kapitolyo, Pasig City",
			"products": [
				"1 Unit / 5 stops <b>GREAT</b> Passenger Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>MR. TEM CHENG RESIDENCE</b>",
			"address": "Sta. Catalina St., Quezon City",
			"products": [
				"1 Unit / 3 stops <b>GREAT</b> Homelift Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>DR. JOSE N. RODRIGUEZ MEMORIAL HOSPITAL</b>",
			"address": "Tala, Caloocan City",
			"products": [
				"1 Unit / 3 Stops <b>GREAT</b> Hospital Bed Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>AZIZA HOTEL</b>",
			"address": "Puerto Princesa Palawan",
			"products": [
				"1 Units / 2 Stops <b>GREAT</b> Scenic Passenger Elevator",
				"2 Units / 2 Stops <b>AGP</b> Dumbwaiter"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>ALPHA LAND</b>",
			"address": "Ayala Makati City",
			"products": [
				"250 Units <b>AG</b> TWO POST” Carlift"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>TORRE DE LORENZO</b>",
			"address": "Taft Ave. Manila",
			"products": [
				"36 Units <b>AG</b> TWO POST” Carlift"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>NATIONAL PRINTING OFFICE</b>",
			"address": "Edsa, NPO road Quezon City",
			"products": [
				"2 Units / 4 Stops <b>HYUNDAI</b> Modernization"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>BOYSEN PAINT PHILIPPINES</b>",
			"address": "Batangas City",
			"products": [
				"5 Units / 4 Stops <b>GREAT</b> Freight Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>GKK INC.</b>",
			"address": "Paco Manila",
			"products": [
				"1 Unit / 7 Stops <b>GREAT</b> Passenger Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>GROWRICH MFG. INC.</b>",
			"address": "Congressional Ave. Quezon City",
			"products": [
				"1 Unit / 4 Stops <b>GREAT</b> Home Lift"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>AMWAY PHILIPPINES</b>",
			"address": "CDO",
			"products": [
				"2 Units / 3 Stops <b>AGP</b> Dumbwaiter"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>FONTANA LEISURE PARK</b>",
			"address": "Angeles, Pampanga",
			"products": [
				"4 Units / 2 Stops <b>AGP</b> Dumbwaiter"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>MRS. GRINO BLDG.</b>",
			"address": "Sampaloc Manila",
			"products": [
				"1 Unit / 6 Stops <b>GREAT</b> Passenger Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>REGATTA RESIDENCES</b>",
			"address": "San Juan Manila",
			"products": [
				"12 Units <b>AG</b> SINGLE/FOUR POST” Carlift"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>CEDAR COMMODITIES</b>",
			"address": "Quezon City",
			"products": [
				"19 Units <b>AG</b> FOUR POST” Carlift"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>MANILA OCEAN PARK</b>",
			"address": "Luneta Park Manila",
			"products": [
				"1 Unit / 2 Stops <b>GREAT</b> Freight Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>STA. TERESITA HOSPITAL</b>",
			"address": "Marcos, Ilocos Norte",
			"products": [
				"1 Unit / 5 Stops <b>GREAT</b> Hospital Bed Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>POEA</b>",
			"address": "Edsa Cor.Ortigas Mandaluyong City",
			"products": [
				"1 Unit / 7 Stops <b>GREAT</b> Passenger Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>BUREAU OF INTERNAL REVENUE</b>",
			"address": "Diliman Quezon City",
			"products": [
				"2 Units / 13 Stops <b>GREAT</b> Passenger Elevator",
				"1 Unit / 3 Stops <b>GREAT</b> Passenger Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>CALAPAN PUBLIC MARKET</b>",
			"address": "Calapan, Mindoro",
			"products": [
				"2 Units <b>GREAT</b> Escalator",
				"1 Unit / 2 Stops <b>GREAT</b> Freight / Passenger Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>AMKOR TECHNOLOGY PHILIPPINES</b>",
			"address": "Cupang, Muntinlupa City",
			"products": [
				"1 Unit / 2 Stops <b>GREAT</b> Freight / Passenger Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>JULIANA&rsquo;S APARTMENT</b>",
			"address": "Doroteo Jose Manila",
			"products": [
				"1 Unit / 8 Stops <b>GREAT</b> Passenger Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>MARIANO MARCOS MEDICAL CENTER</b>",
			"address": "Batac, Ilocos Norte",
			"products": [
				"1 Unit / 5 Stops <b>GREAT</b> Bed Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>MAKATI MEDICAL CENTER</b>",
			"address": "Makati City",
			"products": [
				"1 Unit / 12 Stops <b>GREAT</b> Freight / Passenger Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>GMA WATER DISTICT</b>",
			"address": "GMA Cavite",
			"products": [
				"1 Unit / 3 Stops <b>GREAT</b> Scenic Passenger Elevator"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>MARIANO MARCOS MEDICAL CENTER</b>",
			"address": "Batac, Ilocos Norte",
			"products": [
				"2 Unit / 4 Stops <b>GREAT</b> Bed Elevator 1600 kg"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>VILLA CACERES HOTEL</b>",
			"address": "Magsaysay Avenue Naga City",
			"products": [
				"1 Unit / 4 Stops <b>GREAT</b> Passenger Elevator 1000",
				"1 Unit / 4 Stops <b>GREAT</b> Scenic Elevator 1000 kg",
				"1 Unit / 4 Stops <b>GREAT</b> Freight Elevator 1000 kg"
			],
			"services": "Installation / Maintenance"
		},
		{
			"company": "<b>STARVIEW PLAZA HOTEL</b>",
			"address": "Elias Angeles St., Naga City",
			"products": [
				"1 Unit / 7 Stops <b>GREAT</b> Passenger Elevator 750 kg"
			],
			"services": "Modernization / VVVF Control"
		},
		{
			"company": "<b>ARCHBISHOP PALACE DE CACERES</b>",
			"address": "Elias Angeles St., Naga City",
			"products": [
				"1 Units / 2 Stops <b>GREAT</b> Homelift 280 kg"
			],
			"services": "Local Fabrication"
		}
	];
});

jmciApp.controller('AffiliatesController', function ($scope){
});

jmciApp.controller('ContactController', function ($scope){
});
